function create-db-doc() { # Database定義書を作成する
  env-local
  db-mk
  db-migrate
  db-doc
  db-rm
}

function help(){ # コマンド一覧を表示する
    cat ./CreateDBDoc.bash | grep "^function [A-z]"
}

######################################
# 以下 個別ファンクション
######################################

# ローカル設定
function env-local() {
  export container_name=db-local
  export db_name=mydb
  export db_port=3306
  export db_pwd=password
  env-flyway
}

# flyway用設定
function env-flyway() {
  export SPRING_DATASOURCE_URL=jdbc:mysql://localhost:$db_port/$db_name
  export SPRING_DATASOURCE_USERNAME=root
  export SPRING_DATASOURCE_PASSWORD=$db_pwd
}

# DB doc を開く
function open-db-doc() {
  start $(pwd)/doc/DB/index.html
}

# docker-network作成
# コンテナ間通信を行うために必要
function docker-network() {
  docker network create --driver bridge mynet
}

# DB(docker)を作成する
function db-mk() {
  # containerを削除する
  db-rm

  # mysqlを起動する
  docker run --rm \
    --net="mynet" \
    --name $container_name \
    -p $db_port:3306 \
    -e MYSQL_DATABASE=$db_name \
    -e MYSQL_ROOT_HOST=% \
    -e MYSQL_ROOT_PASSWORD=$db_pwd \
    -d mysql:5.7 \
    --character-set-server=utf8mb4 \
    --collation-server=utf8mb4_unicode_ci

  # mysqlの起動を待つ
  docker run --rm --net="mynet" --name dockerize jwilder/dockerize -wait tcp://$container_name:3306 -timeout 300s
}

# DB(docker)を削除する
function db-rm() {
  # containerが起動していたら停止する
  if [ "$(docker container ls -q -f name=$container_name)" ]; then
    docker stop $container_name
  fi

  # containerが存在していたら削除する
  if [ "$(docker container ls -a -q -f name=$container_name)" ]; then
    docker rm $container_name
  fi
}

# ローカルDBをマイグレーションする
function db-migrate() {
  sh gradlew flywayMigrate
  sh gradlew flywayInfo
}

# DBマイグレーションを修復する
function db-repair() {
  sh gradlew flywayRepair
  sh gradlew flywayValidate
}

# DBマイグレーションを最初からやり直す
function db-clean() {
  sh gradlew flywayClean
  sh gradlew flywayMigrate
  sh gradlew flywayInfo
  sh gradlew flywayRepair
  sh gradlew flywayValidate
}

# DataBase定義書を作成する
function db-doc() {
  # export dir_doc_schema=$(pwd -W)/doc/DB
  export dir_doc_schema=doc/DB

  mkdir ${dir_doc_schema}

  rm -rf ${dir_doc_schema}/*

  docker run --rm \
    --net mynet \
    -v ${dir_doc_schema}:/output \
    schemaspy/schemaspy:snapshot \
    -t mysql \
    -host ${container_name}:3306 \
    -db ${db_name} \
    -u root \
    -p ${db_pwd} \
    -s ${db_name}
}

######################################
# 以下 実行用
######################################

# 引数に指定した関数を実行する
for x in "$@"; do
  set -x
  $x
  set +x
done

# 引数が指定されない場合はcreate-db-docを実行する
if [ -z "$@" ]; then
    set -x
    create-db-doc
    set +x
fi
