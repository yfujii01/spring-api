set -x

# schemaspyインストール
wget https://github.com/schemaspy/schemaspy/releases/download/v6.1.0/schemaspy-6.1.0.jar

# mysqlインストール
apt-get update
debconf-set-selections <<< 'mysql-server mysql-server/root_password password your_password'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password your_password'
apt-get -y install mysql-server

# mysqlスタート
service mysql start

# Database作成
mysql -uroot -pyour_password -e'create database mydb;'

# flyway設定
export SPRING_DATASOURCE_URL=jdbc:mysql://localhost:3306/mydb
export SPRING_DATASOURCE_USERNAME=root
export SPRING_DATASOURCE_PASSWORD=your_password

# flywayでマイグレーション
sh gradlew flywayMigrate

# 図作成用ツールインストール
apt-get install graphviz -y

# mysqlの接続ドライバーインストール
apt install libmysql-java -y

# 定義書作成
java -jar schemaspy-6.1.0.jar \
	-t mysql \
	-db mydb \
	-s mydb \
	-host localhost:3306 \
	-u root \
	-p your_password \
	-dp /usr/share/java/mysql.jar \
	-o ./db

set +x
