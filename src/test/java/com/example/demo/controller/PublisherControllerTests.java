package com.example.demo.controller;

import com.example.demo.entity.Publisher;
import com.example.demo.exception.NoDataException;
import com.example.demo.repository.PublisherRepository;
import com.example.demo.security.SecurityConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PublisherControllerTests extends AbstractControllerTests {
  String token;
  @Autowired PublisherRepository PublisherRepository;

  /** mockを作成する */
  @Before
  public void setup() {
    super.setup();

    String username = "root";
    String password = "password";

    try {
      token = getTokenString(username, password);
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail();
    }
  }

  @Test
  public void findAll() throws Exception {
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/publishers") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    List<Publisher> Publishers = mapper.readValue(body, new TypeReference<List<Publisher>>() {});

    Assert.assertEquals(4, Publishers.size());
  }

  @Test
  public void findById() throws Exception {
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/publishers/1") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    Publisher Publisher = mapper.readValue(body, Publisher.class);

    Publisher expected = new Publisher();
    expected.setId(new Long(1));
    expected.setName("アイカム");

    Assert.assertEquals(expected.getName(), Publisher.getName());
  }

  @Test
  public void insert() throws Exception {

    // テスト実行前のPublisher
    List<Publisher> beforePublishers = PublisherRepository.findAll();

    Publisher insertPublisher =
        new Publisher() {
          {
            setName("ほげ");
          }
        };

    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                        .post("/publishers") // url
                        .header(SecurityConstants.HEADER_STRING, token) // header
                    )
                    .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                    .content(new ObjectMapper().writeValueAsString(insertPublisher)) // パラメータJsonの設定
                )
            .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    Long id = mapper.readValue(body, Long.class);

    // テスト実行後のPublisher
    List<Publisher> afterPublishers = PublisherRepository.findAll();

    Assert.assertEquals(beforePublishers.size() + 1, afterPublishers.size());
    Assert.assertEquals(beforePublishers.size() + 1, id.intValue());
  }

  @Test
  public void update() throws Exception {

    Publisher updatePublisher =
        new Publisher() {
          {
            setId(new Long(1));
            setName("ほげ");
          }
        };

    // テスト実行前のPublisher
    Publisher beforePublisher = PublisherRepository.findById(updatePublisher);

    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                        .put("/publishers/1") // url
                        .header(SecurityConstants.HEADER_STRING, token) // header
                    )
                    .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                    .content(new ObjectMapper().writeValueAsString(updatePublisher)) // パラメータJsonの設定
                )
            .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    // テスト実行後のPublisher
    Publisher afterPublisher = mapper.readValue(body, Publisher.class);

    Assert.assertNotEquals(beforePublisher.toString(), afterPublisher.toString());
    Assert.assertEquals(afterPublisher.getName(), updatePublisher.getName());
  }

  @Test
  public void delete() throws Exception {

    // テスト実行前のPublisher
    List<Publisher> beforePublishers = PublisherRepository.findAll();

    Publisher deletePublisher =
        new Publisher() {
          {
            setId(new Long(1));
          }
        };

    mockMvc
        .perform(
            (MockMvcRequestBuilders //
                .delete("/publishers/1") // url
                .header(SecurityConstants.HEADER_STRING, token) // header
            ))
        .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
        .andReturn();

    // テスト実行後のPublisher
    List<Publisher> afterPublishers = PublisherRepository.findAll();

    Assert.assertEquals(beforePublishers.size() - 1, afterPublishers.size());

    // 存在しないデータを探しに行くためNoDataExceptionが発生する
    try {
      PublisherRepository.findById(deletePublisher);

      // 例外発生しない = データが取得できた = データが消せていないので test ng
      Assert.fail();
    } catch (NoDataException e) {
      // test ok
    }
  }
}
