package com.example.demo.controller;

import com.example.demo.entity.Authority;
import com.example.demo.entity.User;
import com.example.demo.exception.NoDataException;
import com.example.demo.repository.UserRepository;
import com.example.demo.security.SecurityConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTests extends AbstractControllerTests {
  String token;
  @Autowired UserRepository userRepository;

  /** mockを作成する */
  @Before
  public void setup() {
    super.setup();

    String username = "root";
    String password = "password";

    try {
      token = getTokenString(username, password);
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail();
    }
  }

  @Test
  public void findAll() throws Exception {
    // public呼び出し
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/users") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    List<User> Users = mapper.readValue(body, new TypeReference<List<User>>() {});

    Assert.assertEquals(3, Users.size());
  }

  @Test
  public void findById() throws Exception {
    // public呼び出し
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/users/root") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    User user = mapper.readValue(body, User.class);

    User expected =
        new User() {
          {
            setUsername("root");
            setPassword("");
            setEnabled(true);
            setAuthorities(
                new ArrayList<Authority>() {
                  private static final long serialVersionUID = 1L;

                  {
                    add(
                        new Authority() {
                          {
                            setUsername("root");
                            setAuthority("ADMIN");
                          }
                        });
                  }
                });
          }
        };

    // パスワードはハッシュ化されているため比較しない
    user.setPassword("");
    Assert.assertEquals(expected.toString(), user.toString());
  }

  @Test
  public void insert() throws Exception {

    // テスト実行前のUser
    List<User> beforeUsers = userRepository.findAll();

    User insertUser =
        new User() {
          {
            setUsername("hoge");
            setPassword("password");
            setEnabled(true);
            setAuthorities(
                new ArrayList<Authority>() {
                  private static final long serialVersionUID = 1L;

                  {
                    add(
                        new Authority() {
                          {
                            setUsername("hoge");
                            setAuthority("ADMIN");
                          }
                        });
                    add(
                        new Authority() {
                          {
                            setUsername("hoge");
                            setAuthority("USER");
                          }
                        });
                  }
                });
          }
        };

    // public呼び出し
    mockMvc
        .perform(
            (MockMvcRequestBuilders //
                    .post("/users") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                )
                .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                .content(new ObjectMapper().writeValueAsString(insertUser)) // パラメータJsonの設定
            )
        .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
        .andReturn();

    // テスト実行後のUser
    List<User> afterUsers = userRepository.findAll();

    Assert.assertEquals(beforeUsers.size() + 1, afterUsers.size());
  }

  @Test
  public void update() throws Exception {

    User updateUser =
        new User() {
          {
            setUsername("guest");
            setPassword("");
            setEnabled(false);
            setAuthorities(
                new ArrayList<Authority>() {
                  private static final long serialVersionUID = 1L;

                  {
                    add(
                        new Authority() {
                          {
                            setUsername("guest");
                            setAuthority("GUEST");
                          }
                        });
                  }
                });
          }
        };

    // テスト実行前のUser
    User beforeUser = userRepository.findById(updateUser);

    // public呼び出し
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                        .put("/users/guest") // url
                        .header(SecurityConstants.HEADER_STRING, token) // header
                    )
                    .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                    .content(new ObjectMapper().writeValueAsString(updateUser)) // パラメータJsonの設定
                )
            .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    // テスト実行後のUser
    User afterUser = mapper.readValue(body, User.class);

    Assert.assertNotEquals(beforeUser.toString(), afterUser.toString());
    Assert.assertEquals(afterUser.toString(), updateUser.toString());
  }

  @Test
  public void delete() throws Exception {

    // テスト実行前のUser
    List<User> beforeUsers = userRepository.findAll();

    User deleteUser =
        new User() {
          {
            setUsername("guest");
          }
        };

    // public呼び出し
    mockMvc
        .perform(
            (MockMvcRequestBuilders //
                .delete("/users/guest") // url
                .header(SecurityConstants.HEADER_STRING, token) // header
            ))
        .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
        .andReturn();

    // テスト実行後のUser
    List<User> afterUsers = userRepository.findAll();

    Assert.assertEquals(beforeUsers.size() - 1, afterUsers.size());

    // 存在しないデータを探しに行くためNoDataExceptionが発生する
    try {
      userRepository.findById(deleteUser);

      // 例外発生しない = データが取得できた = データが消せていないので test ng
      Assert.fail();
    } catch (NoDataException e) {
      // test ok
    }
  }
}
