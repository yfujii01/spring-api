package com.example.demo.controller;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import com.example.demo.entity.Publisher;
import com.example.demo.exception.NoDataException;
import com.example.demo.repository.BookRepository;
import com.example.demo.security.SecurityConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookControllerTests extends AbstractControllerTests {
  String token;
  @Autowired BookRepository bookRepository;

  /** mockを作成する */
  @Before
  public void setup() {
    super.setup();

    String username = "root";
    String password = "password";

    try {
      token = getTokenString(username, password);
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail();
    }
  }

  @Test
  public void findAll() throws Exception {
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/books") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    List<Book> books = mapper.readValue(body, new TypeReference<List<Book>>() {});

    Assert.assertEquals(11, books.size());
  }

  @Test
  public void findById() throws Exception {
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/books/1") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    Book book = mapper.readValue(body, Book.class);

    Book expected = new Book();
    expected.setId(new Long(1));
    expected.setTitle("ゆめをかなえるぞう");
    expected.setIsbn("651324212");
    expected.setPublicationYear("2017");
    expected.setPubId(new Long(1));
    expected.setPublisher(
        new Publisher() {
          {
            setId(new Long(1));
            setName("アイカム");
          }
        });
    expected.setAuthors(
        new ArrayList<Author>() {
          private static final long serialVersionUID = 1L;

          {
            add(
                new Author() {
                  {
                    setId(new Long(1));
                    setLastName("さとう");
                    setFirstName("たける");
                  }
                });
            add(
                new Author() {
                  {
                    setId(new Long(2));
                    setLastName("たなか");
                    setFirstName("ごろう");
                  }
                });
          }
        });

    Assert.assertEquals(expected.toString(), book.toString());
  }

  @Test
  public void insert() throws Exception {

    // テスト実行前のbook
    List<Book> beforeBooks = bookRepository.findAll();

    Book insertBook =
        new Book() {
          {
            setTitle("title");
            setIsbn("isbn");
            setPublicationYear("2019");
          }
        };

    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                        .post("/books") // url
                        .header(SecurityConstants.HEADER_STRING, token) // header
                    )
                    .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                    .content(new ObjectMapper().writeValueAsString(insertBook)) // パラメータJsonの設定
                )
            .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    Long id = mapper.readValue(body, Long.class);

    // テスト実行後のbook
    List<Book> afterBooks = bookRepository.findAll();

    Assert.assertEquals(beforeBooks.size() + 1, afterBooks.size());
    Assert.assertEquals(beforeBooks.size() + 1, id.intValue());
  }

  @Test
  public void update() throws Exception {

    Book updateBook =
        new Book() {
          {
            setId(new Long(1));
            setTitle("編集後タイトル");
            setIsbn("7777777");
            setPublicationYear("1999");
            setPubId((long) 2);
          }
        };

    // テスト実行前のBook
    Book beforeBook = bookRepository.findById(updateBook);

    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                        .put("/books/1") // url
                        .header(SecurityConstants.HEADER_STRING, token) // header
                    )
                    .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                    .content(new ObjectMapper().writeValueAsString(updateBook)) // パラメータJsonの設定
                )
            .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    // テスト実行後のBook
    Book afterBook = mapper.readValue(body, Book.class);

    Assert.assertNotEquals(beforeBook.toString(), afterBook.toString());

    // 関連項目(著者,出版社)は作成していないが、自動で紐づいて取得されるためテスト対象から除く
    afterBook.setAuthors(null);
    afterBook.setPublisher(null);
    Assert.assertEquals(afterBook.toString(), updateBook.toString());
  }

  @Test
  public void delete() throws Exception {

    // テスト実行前のBook
    List<Book> beforeBooks = bookRepository.findAll();

    Book deleteBook =
        new Book() {
          {
            setId(new Long(1));
          }
        };

    mockMvc
        .perform(
            (MockMvcRequestBuilders //
                .delete("/books/1") // url
                .header(SecurityConstants.HEADER_STRING, token) // header
            ))
        .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
        .andReturn();

    // テスト実行後のBook
    List<Book> afterBooks = bookRepository.findAll();

    Assert.assertEquals(beforeBooks.size() - 1, afterBooks.size());

    // 存在しないデータを探しに行くためNoDataExceptionが発生する
    try {
      bookRepository.findById(deleteBook);

      // 例外発生しない = データが取得できた = データが消せていないので test ng
      Assert.fail();
    } catch (NoDataException e) {
      // test ok
    }
  }

  /** 本と著者の関連付け */
  @Test
  public void authorInsert() throws Exception {

    Book book =
        new Book() {
          {
            setId((long) 1);
          }
        };
    // テスト実行前のBook
    Book beforeBook = bookRepository.findById(book);

    mockMvc
        .perform(
            (MockMvcRequestBuilders //
                .post("/books/1/authors/3") // url
                .header(SecurityConstants.HEADER_STRING, token) // header
            ))
        .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
        .andReturn();

    // テスト実行後のbook
    Book afterBook = bookRepository.findById(book);

    Assert.assertEquals(beforeBook.getAuthors().size() + 1, afterBook.getAuthors().size());
  }

  /** 本と著者の関連付け削除 */
  @Test
  public void authorDelete() throws Exception {

    Book book =
        new Book() {
          {
            setId((long) 1);
          }
        };
    // テスト実行前のBook
    Book beforeBook = bookRepository.findById(book);

    mockMvc
        .perform(
            (MockMvcRequestBuilders //
                .delete("/books/1/authors/1") // url
                .header(SecurityConstants.HEADER_STRING, token) // header
            ))
        .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
        .andReturn();

    // テスト実行後のbook
    Book afterBook = bookRepository.findById(book);

    Assert.assertEquals(beforeBook.getAuthors().size() - 1, afterBook.getAuthors().size());
  }
}
