package com.example.demo.controller;

import com.example.demo.entity.Author;
import com.example.demo.exception.NoDataException;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.security.SecurityConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthorControllerTests extends AbstractControllerTests {
  String token;
  @Autowired AuthorRepository AuthorRepository;

  /** mockを作成する */
  @Before
  public void setup() {
    super.setup();

    String username = "root";
    String password = "password";

    try {
      token = getTokenString(username, password);
    } catch (Exception e) {
      e.printStackTrace();
      Assert.fail();
    }
  }

  @Test
  public void findAll() throws Exception {
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/authors") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    List<Author> Authors = mapper.readValue(body, new TypeReference<List<Author>>() {});

    Assert.assertEquals(8, Authors.size());
  }

  @Test
  public void findById() throws Exception {
    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                    .get("/authors/1") // url
                    .header(SecurityConstants.HEADER_STRING, token) // header
                ))
            // 返却されるhttp status code
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    Author Author = mapper.readValue(body, Author.class);

    Author expected = new Author();
    expected.setId(new Long(1));
    expected.setFirstName("たける");
    expected.setLastName("さとう");

    Assert.assertEquals(expected.getFirstName(), Author.getFirstName());
    Assert.assertEquals(expected.getLastName(), Author.getLastName());
  }

  @Test
  public void insert() throws Exception {

    // テスト実行前のAuthor
    List<Author> beforeAuthors = AuthorRepository.findAll();

    Author insertAuthor =
        new Author() {
          {
            setFirstName("たろう");
            setLastName("てすと");
          }
        };

    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                        .post("/authors") // url
                        .header(SecurityConstants.HEADER_STRING, token) // header
                    )
                    .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                    .content(new ObjectMapper().writeValueAsString(insertAuthor)) // パラメータJsonの設定
                )
            .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    Long id = mapper.readValue(body, Long.class);

    // テスト実行後のAuthor
    List<Author> afterAuthors = AuthorRepository.findAll();

    Assert.assertEquals(beforeAuthors.size() + 1, afterAuthors.size());
    Assert.assertEquals(beforeAuthors.size() + 1, id.intValue());
  }

  @Test
  public void update() throws Exception {

    Author updateAuthor =
        new Author() {
          {
            setId(new Long(1));
            setFirstName("たろう");
            setLastName("てすと");
          }
        };

    // テスト実行前のAuthor
    Author beforeAuthor = AuthorRepository.findById(updateAuthor);

    MvcResult result =
        mockMvc
            .perform(
                (MockMvcRequestBuilders //
                        .put("/authors/1") // url
                        .header(SecurityConstants.HEADER_STRING, token) // header
                    )
                    .contentType(MediaType.APPLICATION_JSON) // ContentTypeの設定
                    .content(new ObjectMapper().writeValueAsString(updateAuthor)) // パラメータJsonの設定
                )
            .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
            .andReturn();

    // body内容の確認
    String body = result.getResponse().getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    // テスト実行後のAuthor
    Author afterAuthor = mapper.readValue(body, Author.class);

    Assert.assertNotEquals(beforeAuthor.toString(), afterAuthor.toString());
    Assert.assertEquals(afterAuthor.getFirstName(), updateAuthor.getFirstName());
    Assert.assertEquals(afterAuthor.getLastName(), updateAuthor.getLastName());
  }

  @Test
  public void delete() throws Exception {

    // テスト実行前のAuthor
    List<Author> beforeAuthors = AuthorRepository.findAll();

    Author deleteAuthor =
        new Author() {
          {
            setId(new Long(1));
          }
        };

    mockMvc
        .perform(
            (MockMvcRequestBuilders //
                .delete("/authors/1") // url
                .header(SecurityConstants.HEADER_STRING, token) // header
            ))
        .andExpect(MockMvcResultMatchers.status().isOk()) // 返却されるhttp status code
        .andReturn();

    // テスト実行後のAuthor
    List<Author> afterAuthors = AuthorRepository.findAll();

    Assert.assertEquals(beforeAuthors.size() - 1, afterAuthors.size());

    // 存在しないデータを探しに行くためNoDataExceptionが発生する
    try {
      AuthorRepository.findById(deleteAuthor);

      // 例外発生しない = データが取得できた = データが消せていないので test ng
      Assert.fail();
    } catch (NoDataException e) {
      // test ok
    }
  }
}
