package com.example.demo.repository;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import com.example.demo.exception.NoDataException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorRepository {

  @Autowired NamedParameterJdbcTemplate jdbcTemplateName;

  @Autowired JdbcTemplate jdbcTemplate;

  /** 全件検索 */
  public List<Author> findAll() {
    // パラメータ設定
    final SqlParameterSource params = null;
    final StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append("   a.*");
    sql.append(" from author a");
    sql.append(" order by");
    sql.append("   a.id asc");

    // SQL発行
    return execute(params, sql);
  }

  /** ID指定で検索 */
  public Author findById(Author author) throws NoDataException {
    // パラメータ設定
    final SqlParameterSource params =
        new MapSqlParameterSource()
            // ID
            .addValue("id", author.getId());

    final StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append("   a.*,");
    sql.append("   b.id book_id,");
    sql.append("   b.title book_title,");
    sql.append("   b.isbn book_isbn,");
    sql.append("   b.publication_year book_publication_year,");
    sql.append("   b.pub_id book_pub_id");
    sql.append(" from author a");
    sql.append(" left outer join writing w");
    sql.append(" on a.id = w.author_id");
    sql.append(" left outer join book b");
    sql.append(" on b.id = w.book_id");
    sql.append(" where");
    sql.append("   a.id = :id");

    // SQL発行
    List<Author> execute = execute(params, sql);
    if (execute.size() == 0) {
      throw new NoDataException("Data Not Found!");
    }
    return execute.get(0);
  }

  /** 登録 */
  public Long insert(Author author) {
    // パラメータ設定
    SqlParameterSource params =
        new MapSqlParameterSource()
            // 名前
            .addValue("first_name", author.getFirstName())
            // 苗字
            .addValue("last_name", author.getLastName());

    final StringBuilder sql = new StringBuilder();
    sql.append(" insert into author set");
    sql.append("   first_name = :first_name,");
    sql.append("   last_name = :last_name");
    jdbcTemplateName.update(sql.toString(), params);

    // 登録したレコードのIDを返却
    return jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Long.class);
  }

  /**
   * 修正
   *
   * @throws NoDataException
   */
  public Author update(Author author) throws NoDataException {
    // パラメータ設定
    SqlParameterSource params =
        new MapSqlParameterSource()
            // ID
            .addValue("id", author.getId())
            // 名前
            .addValue("first_name", author.getFirstName())
            // 苗字
            .addValue("last_name", author.getLastName());

    // SQL発行
    final StringBuilder sql = new StringBuilder();
    sql.append(" update author set");
    sql.append("   first_name = :first_name,");
    sql.append("   last_name = :last_name");
    sql.append(" where");
    sql.append("   id = :id");
    jdbcTemplateName.update(sql.toString(), params);

    // 返却
    return this.findById(author);
  }

  /** 削除 */
  public void delete(Author author) {
    // パラメータ設定
    final SqlParameterSource params =
        new MapSqlParameterSource()
            // ID
            .addValue("id", author.getId());

    final StringBuilder sql = new StringBuilder();
    sql.append(" delete from author");
    sql.append(" where");
    sql.append("   id = :id");

    // SQL発行
    jdbcTemplateName.update(sql.toString(), params);
  }

  private List<Author> execute(final SqlParameterSource params, final StringBuilder sql) {
    final Map<Long, Author> authorMap = new TreeMap<>();
    jdbcTemplateName
        .queryForList(sql.toString(), params)
        .forEach(
            res -> {
              final Long key = (Long) res.get("id");

              if (!authorMap.containsKey(key)) {
                // 新規

                final Author value = new Author();
                authorMap.put(key, value);
                value.setId((Long) res.get("id"));
                value.setFirstName((String) res.get("first_name"));
                value.setLastName((String) res.get("last_name"));

                // (関連)本情報
                value.setBooks(
                    new ArrayList<Book>() {
                      private static final long serialVersionUID = 1L;

                      {
                        add(
                            new Book() {
                              {
                                setId((Long) res.get("book_id"));
                                setTitle((String) res.get("book_title"));
                                setIsbn((String) res.get("book_isbn"));
                                setPublicationYear((String) res.get("book_publication_year"));
                                setPubId((Long) res.get("book_pub_id"));
                              }
                            });
                      }
                    });
              } else {
                // 関連データ追加

                final Author value = authorMap.get(key);

                // (関連)本情報(2件目以降)
                value
                    .getBooks()
                    .add(
                        new Book() {
                          {
                            setId((Long) res.get("book_id"));
                            setTitle((String) res.get("book_title"));
                            setIsbn((String) res.get("book_isbn"));
                            setPublicationYear((String) res.get("book_publication_year"));
                            setPubId((Long) res.get("book_pub_id"));
                          }
                        });
              }
            });
    return new ArrayList<>(authorMap.values());
  }
}
