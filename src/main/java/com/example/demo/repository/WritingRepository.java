package com.example.demo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class WritingRepository {

  @Autowired NamedParameterJdbcTemplate jdbcTemplateName;

  @Autowired JdbcTemplate jdbcTemplate;

  /** 登録 */
  public void insert(long bookId, long authorId) {
    // パラメータ設定
    SqlParameterSource params =
        new MapSqlParameterSource()
            // 本ID
            .addValue("book_id", bookId)
            // 著者ID
            .addValue("author_id", authorId);

    final StringBuilder sql = new StringBuilder();
    sql.append(" insert into writing set");
    sql.append("   book_id = :book_id,");
    sql.append("   author_id = :author_id");
    jdbcTemplateName.update(sql.toString(), params);
  }

  /** 削除 */
  public void delete(long bookId, long authorId) {
    // パラメータ設定
    final SqlParameterSource params =
        new MapSqlParameterSource()
            // 本ID
            .addValue("book_id", bookId)
            // 著者ID
            .addValue("author_id", authorId);

    final StringBuilder sql = new StringBuilder();
    sql.append(" delete from writing");
    sql.append(" where");
    sql.append("   book_id = :book_id");
    sql.append(" and author_id = :author_id");

    // SQL発行
    jdbcTemplateName.update(sql.toString(), params);
  }
}
