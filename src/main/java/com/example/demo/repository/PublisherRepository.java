package com.example.demo.repository;

import com.example.demo.entity.Book;
import com.example.demo.entity.Publisher;
import com.example.demo.exception.NoDataException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class PublisherRepository {

  @Autowired NamedParameterJdbcTemplate jdbcTemplateName;

  @Autowired JdbcTemplate jdbcTemplate;

  /** 全件検索 */
  public List<Publisher> findAll() {
    // パラメータ設定
    final SqlParameterSource params = null;
    final StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append("   p.*");
    sql.append(" from publisher p");
    sql.append(" order by");
    sql.append("   p.id desc");

    // SQL発行
    return execute(params, sql);
  }

  /** ID指定で検索 */
  public Publisher findById(Publisher publisher) throws NoDataException {
    // パラメータ設定
    final SqlParameterSource params =
        new MapSqlParameterSource()
            // ID
            .addValue("id", publisher.getId());

    final StringBuilder sql = new StringBuilder();
    sql.append(" select");
    sql.append("   p.*,");
    sql.append("   b.id book_id,");
    sql.append("   b.title book_title,");
    sql.append("   b.isbn book_isbn,");
    sql.append("   b.publication_year book_publication_year,");
    sql.append("   b.pub_id book_pub_id");
    sql.append(" from publisher p");
    sql.append(" left outer join book b");
    sql.append("   on p.id = b.pub_id");
    sql.append(" where");
    sql.append("   p.id = :id");

    // SQL発行
    List<Publisher> execute = execute(params, sql);
    if (execute.size() == 0) {
      throw new NoDataException("Data Not Found!");
    }
    return execute.get(0);
  }

  /** 登録 */
  public Long insert(Publisher publisher) {
    // パラメータ設定
    SqlParameterSource params =
        new MapSqlParameterSource()
            // 名称
            .addValue("name", publisher.getName());

    final StringBuilder sql = new StringBuilder();
    sql.append(" insert into publisher set");
    sql.append("   name = :name");
    jdbcTemplateName.update(sql.toString(), params);

    // 登録したレコードのIDを返却
    return jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Long.class);
  }

  /**
   * 修正
   *
   * @throws NoDataException
   */
  public Publisher update(Publisher publisher) throws NoDataException {
    // パラメータ設定
    SqlParameterSource params =
        new MapSqlParameterSource()
            // ID
            .addValue("id", publisher.getId())
            // 名称
            .addValue("name", publisher.getName());

    // SQL発行
    final StringBuilder sql = new StringBuilder();
    sql.append(" update publisher set");
    sql.append("   name = :name");
    sql.append(" where");
    sql.append("   id = :id");
    jdbcTemplateName.update(sql.toString(), params);

    // 返却
    return this.findById(publisher);
  }

  /** 削除 */
  public void delete(Publisher publisher) {
    // パラメータ設定
    final SqlParameterSource params =
        new MapSqlParameterSource()
            // ID
            .addValue("id", publisher.getId());

    final StringBuilder sql = new StringBuilder();
    sql.append(" delete from publisher");
    sql.append(" where");
    sql.append("   id = :id");

    // SQL発行
    jdbcTemplateName.update(sql.toString(), params);
  }

  private List<Publisher> execute(final SqlParameterSource params, final StringBuilder sql) {
    final Map<Long, Publisher> map = new TreeMap<>();
    jdbcTemplateName
        .queryForList(sql.toString(), params)
        .forEach(
            res -> {
              final Long key = (Long) res.get("id");

              if (!map.containsKey(key)) {
                // 新規

                final Publisher value = new Publisher();
                map.put(key, value);
                value.setId((Long) res.get("id"));
                value.setName((String) res.get("name"));

                // (関連)本情報
                value.setBooks(
                    new ArrayList<Book>() {
                      private static final long serialVersionUID = 1L;

                      {
                        add(
                            new Book() {
                              {
                                setId((Long) res.get("book_id"));
                                setTitle((String) res.get("book_title"));
                                setIsbn((String) res.get("book_isbn"));
                                setPublicationYear((String) res.get("book_publication_year"));
                                setPubId((Long) res.get("book_pub_id"));
                              }
                            });
                      }
                    });
              } else {
                // 関連データ追加

                final Publisher value = map.get(key);

                // (関連)本情報(2件目以降)
                value
                    .getBooks()
                    .add(
                        new Book() {
                          {
                            setId((Long) res.get("book_id"));
                            setTitle((String) res.get("book_title"));
                            setIsbn((String) res.get("book_isbn"));
                            setPublicationYear((String) res.get("book_publication_year"));
                            setPubId((Long) res.get("book_pub_id"));
                          }
                        });
              }
            });
    return new ArrayList<>(map.values());
  }
}
