package com.example.demo.controller;

import com.example.demo.entity.Publisher;
import com.example.demo.repository.PublisherRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publishers")
public class PublisherController {
  @Autowired PublisherRepository publisherRepository;

  @GetMapping
  public List<Publisher> findAll() throws JsonProcessingException {
    return publisherRepository.findAll();
  }

  @GetMapping("{id}")
  public Publisher findById(@PathVariable("id") Long id) throws Exception {
    Publisher selectPublisher = new Publisher();
    selectPublisher.setId(id);
    return publisherRepository.findById(selectPublisher);
  }

  @PostMapping()
  public Long insert(@Validated @RequestBody Publisher publisher) throws Exception {
    return publisherRepository.insert(publisher);
  }

  @PutMapping("{id}")
  public Publisher update(@PathVariable("id") Long id, @Validated @RequestBody Publisher publisher)
      throws Exception {
    publisher.setId(id);
    return publisherRepository.update(publisher);
  }

  @DeleteMapping("{id}")
  public void delete(@PathVariable("id") Long id) throws Exception {
    Publisher selectPublisher = new Publisher();
    selectPublisher.setId(id);
    publisherRepository.delete(selectPublisher);
  }
}
