package com.example.demo.controller;

import com.example.demo.entity.Book;
import com.example.demo.repository.BookRepository;
import com.example.demo.repository.WritingRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class BookController {
  @Autowired BookRepository bookRepository;

  @Autowired WritingRepository writingRepository;

  @GetMapping
  public List<Book> findAll() throws JsonProcessingException {
    return bookRepository.findAll();
  }

  @GetMapping("{id}")
  public Book findById(@PathVariable("id") Long id) throws Exception {
    Book selectBook = new Book();
    selectBook.setId(id);
    return bookRepository.findById(selectBook);
  }

  @PostMapping()
  public Long insert(@Validated @RequestBody Book book) throws Exception {
    return bookRepository.insert(book);
  }

  @PutMapping("{id}")
  public Book update(@PathVariable("id") Long id, @Validated @RequestBody Book book)
      throws Exception {
    book.setId(id);
    return bookRepository.update(book);
  }

  @DeleteMapping("{id}")
  public void delete(@PathVariable("id") Long id) throws Exception {
    Book selectBook = new Book();
    selectBook.setId(id);
    bookRepository.delete(selectBook);
  }

  @PostMapping("{book_id}/authors/{author_id}")
  public void authorInsert(
      @PathVariable("book_id") Long bookId, @PathVariable("author_id") Long authorId)
      throws Exception {
    writingRepository.insert(bookId, authorId);
  }

  @DeleteMapping("{book_id}/authors/{author_id}")
  public void authorDelete(
      @PathVariable("book_id") Long bookId, @PathVariable("author_id") Long authorId)
      throws Exception {
    writingRepository.delete(bookId, authorId);
  }
}
