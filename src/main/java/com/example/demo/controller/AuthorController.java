package com.example.demo.controller;

import com.example.demo.entity.Author;
import com.example.demo.repository.AuthorRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
public class AuthorController {
  @Autowired AuthorRepository authorRepository;

  @GetMapping
  public List<Author> findAll() throws JsonProcessingException {
    return authorRepository.findAll();
  }

  @GetMapping("{id}")
  public Author findById(@PathVariable("id") Long id) throws Exception {
    Author selectAuthor = new Author();
    selectAuthor.setId(id);
    return authorRepository.findById(selectAuthor);
  }

  @PostMapping()
  public Long insert(@Validated @RequestBody Author author) throws Exception {
    return authorRepository.insert(author);
  }

  @PutMapping("{id}")
  public Author update(@PathVariable("id") Long id, @Validated @RequestBody Author author)
      throws Exception {
    author.setId(id);
    return authorRepository.update(author);
  }

  @DeleteMapping("{id}")
  public void delete(@PathVariable("id") Long id) throws Exception {
    Author selectAuthor = new Author();
    selectAuthor.setId(id);
    authorRepository.delete(selectAuthor);
  }
}
