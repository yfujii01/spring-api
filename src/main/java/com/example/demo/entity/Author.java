package com.example.demo.entity;

import java.util.List;
import lombok.Data;

@Data
public class Author {
  private Long id;
  private String firstName;
  private String lastName;

  /** (関連)本情報 */
  private List<Book> books;
}
