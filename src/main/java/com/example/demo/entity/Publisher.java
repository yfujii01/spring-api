package com.example.demo.entity;

import java.util.List;
import lombok.Data;

@Data
public class Publisher {
  private Long id;
  private String name;
  private List<Book> books;
}
